﻿using System;
using System.Windows.Forms;
using MemoryS.Common;
using MemoryS.Helpers;
using MemoryS.Memory;

namespace MemoryS.Forms
{
    /// <summary>
    /// Главная форма приложения
    /// </summary>
    public partial class Fmain : Form
    {
        bool _dataChanged; // состоянии данных (произошло или их изменение)
        int _success; // кол-во успешных действий
        int _random; // кол-во случайных действий
        Random _rand; // переменная для случайных действий

        private MemoryWorker _memory;

        /// <summary>
        /// Инициализация формы
        /// </summary>
        public Fmain()
        {
            InitializeComponent();
            InitializeStartForm(); // стартовое окно
        }

        #region Инициализация формы и данных
        
        /// <summary>
        /// Инициализация события для показа стартовой формы с просьбой указать размер памяти
        /// </summary>
        private void InitializeStartForm()
        {
            this.Shown += new EventHandler(FStartLoad);
        }

        private void FStartLoad(object sender, EventArgs e)
        {
            new FStart().ShowDialog();
            // обновление полей инофрмации
            InitializeData();
        }

        /// <summary>
        /// Инициализация данных, контролов и памяти
        /// </summary>
        private void InitializeData()
        {
            // инициализация памяти
            _memory = new MemoryWorker(Vars.FullMemSize);

            // настройка данных и контролов
            _success = 0;
            _random = 0;
            _rand = new Random();
            _dataChanged = true;
            textBox1.Text = Vars.FullMemSize.ToString();

            // обновление данных
            RefreshData();
        } 

        #endregion

        /// <summary>
        /// ВЫход из приложения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitExecute(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Открытие окна со справкой о приложении
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutExecute(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        /// <summary>
        /// Добавление процесса в память
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddProcessExecute(object sender, EventArgs e)
        {
            // проверка свободной памяти под процессы (нету вообще или не хватает как минимум на сегемент кода)
            if (_memory.GetFreeMemory == 0 || _memory.GetFreeMemory < Vars.SegmentSize)
            {
                MessageBox.Show("Не хватает памяти под новый процесс!", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // вызов окна добавления процесса
            var form = new FAddProc();
            if (form.ShowDialog() == DialogResult.OK)
            {
                // добавление процесса в память
                _memory.AddProcess(form.ProcName, form.size*1024);

                // обновление формы
                RefreshData();
            }
        }

        /// <summary>
        /// Удаление выбранного процесса из памяти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveProcessExecute(object sender, EventArgs e)
        {
            // проверка выбран ли процесс на удаление
            if (ProcessList.SelectedIndex < 0)
            {
                MessageBox.Show("Не выбран процесс для удаления!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show(ProcessList.Text);
            if (_memory.RemoveProcess(ProcessList.Text)) RefreshData();
        }
       
        /// <summary>
        /// Выполнение случайных действий (добавление/удаление процессов, т. е. выделение/освобождение памяти)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RandomExecute(object sender, EventArgs e)
        {
            // делаем случайное действие из возможных
            int i = _rand.Next();
            if (i % 5 != 0 || _memory.GetProcessCount == 0) // выделяем
            {
                var P = new Process(_random.ToString());
                try
                {
                    _random++;
                    // добавление нового процесса в память
                    _memory.AddProcess(P.Name, _rand.Next()%(Vars.SegmentSize/2));
                    _success++;
                }
                catch (Exception ex)
                {
                    if (P.Segments[0] != null)
                        _memory.FreeSegment(P.Segments[0].Name);
                    if (P.Segments[1] != null)
                        _memory.FreeSegment(P.Segments[1].Name);
                    throw new Exception(ex.Message);
                }
                RefreshData();
            }
            else // освобождаем
            {
                if (_memory.GetProcessCount == 0) return;
                int r = _rand.Next() % _memory.GetProcessCount;
                //RemoveProcess(Proceses[r].Name);
                _memory.SwopProcess(r); // свопим случайный процесс процесс
            }
        }

        /// <summary>
        /// Выполнение записи в память
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WriteExecute(object sender, EventArgs e)
        {
            // вызов окна записи данных
            var form = new FWriteForm();
            form.InitAsWriteForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                // запись данных в память
                _memory.WriteData(form.SegmentName, form.Offset, form.Data);

                // обновление формы
                _dataChanged = true;
                RefreshData();
            }
        }

        /// <summary>
        /// Выполнение чтения из памяти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadExecute(object sender, EventArgs e)
        {
            // вызов окна записи данных
            var form = new FWriteForm();
            form.InitAsReadForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                var str = _memory.ReadData(form.SegmentName, form.Offset, form.Data);
                MessageBox.Show("Прочитано: " + str);
            }
        }

        /// <summary>
        /// Очистка памяти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearMemExecute(object sender, EventArgs e)
        {
            // очистка памяти и удаление из нее процессов
            _memory.Clear();

            // обновление формы
            _dataChanged = true;
            RefreshData();
        }

        /// <summary>
        /// Выбор процесса в списке процессов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessListSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ProcessList.SelectedIndex < 0) return;
            SegmentsList.Items.Clear();
            string name = ProcessList.Items[ProcessList.SelectedIndex] as string;
            var process = _memory.GetProcess(name);
            for (int j = 0; j < process.Segments.Length; j++)
                SegmentsList.Items.Add(process.Segments[j].Name);
        }


        #region Основные функции

        /// <summary>
        /// Обновление данных о памяти и о процессах
        /// </summary>
        private void RefreshData()
        {
            // выводим содержимое памяти
            if (_dataChanged)
            {
                string M = "";
                for (int i = 0; i < _memory.GetMemoryData.Length; i++)
                {
                    if (i%128 == 0 && i != 0)
                        M += '\n';
                    if (i%1024 == 0)
                        M += String.Format("{0} ", (i/1024).ToString().PadLeft(3, '0'));
                    else if (i%128 == 0)
                        M += "    ";
                    M += _memory.GetMemoryData[i];
                }
                _dataChanged = false;
                MemoryDataBox.Text = M;
            }

            // выводим список процессов
            ProcessList.Items.Clear();
            SegmentsList.Items.Clear();

            // процессы в памяти
            foreach (Process P in _memory.Processes)
                ProcessList.Items.Add(P.Name);

            // процессы с свопе
            foreach (Process P in _memory.ProcessInSwop)
                SwopProcessList.Items.Add(P.Name);

            textBox2.Text = _memory.GetFreeMemory.ToString();
            textBox3.Text = _success.ToString();
            textBox4.Text = _random.ToString();
        }

        #endregion
    }
}
