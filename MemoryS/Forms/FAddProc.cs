﻿using System;
using System.Windows.Forms;

namespace MemoryS.Forms
{
    /// <summary>
    /// Форма для создания нового процесса
    /// </summary>
    public partial class FAddProc : Form
    {
        public string ProcName; // имя процесса
        public int size; // размер сегмента данных

        public FAddProc()
        {
            InitializeComponent();
        }
        
        // кнопка OK
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" &&
                textBox1.Text != " " && textBox2.Text != " ")
            {
                ProcName = textBox1.Text;
                size = Convert.ToInt32(textBox2.Text);
                DialogResult = DialogResult.OK;
                Close();
            }
            else
                MessageBox.Show("Не задана информация о процессе!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        
        // кнопка Отмена
        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
