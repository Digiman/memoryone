﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using MemoryS.Common;
using MemoryS.Memory;

namespace MemoryS.Helpers
{
    /// <summary>
    /// Класс для реализации оболочки над памятью для выполнения операций с ней
    /// </summary>
    public class MemoryWorker
    {
        private readonly List<Process> _processes; // процессы
        private readonly SegmentMemory _mem; // абстрактная память (виртуализируемая реальная память)
        private readonly List<Process> _processInSwop; // поцессы в свопе - сделать именно как своп на диске

        /// <summary>
        /// Инициализация памяти, сегментов и процессов
        /// </summary>
        /// <param name="size">Размер памяти (в Кбайт)</param>
        public MemoryWorker(int size)
        {
            _mem = new SegmentMemory(size); // 30 / 30 сегментов (код / данные), размер памяти (Кб)
            _processes = new List<Process>(); // начальное состояние процессов
            _processInSwop = new List<Process>();
        }

        /// <summary>
        /// Количество свободной памяти в текущий момент
        /// </summary>
        public int GetFreeMemory
        {
            get { return _mem.FreeMem; }
        }

        /// <summary>
        /// Получение количества процессов в памяти
        /// </summary>
        public int GetProcessCount
        {
            get { return Processes.Count; }
        }

        /// <summary>
        /// Список процессов в памяти
        /// </summary>
        public List<Process> Processes
        {
            get { return _processes; }
        }

        /// <summary>
        /// Список процессов в свопе
        /// </summary>
        public List<Process> ProcessInSwop
        {
            get { return _processInSwop; }
        }

        /// <summary>
        /// Полученеи содержимого памяти
        /// </summary>
        public char[] GetMemoryData
        {
            get { return _mem.MemoryData; }
        }

        /// <summary>
        /// Получение процесса по его имени
        /// </summary>
        /// <param name="processName">Имя процесса</param>
        /// <returns>Возвращает найденный процесс</returns>
        public Process GetProcess(string processName)
        {
            return Processes.SingleOrDefault(p => p.Name == processName);
        }

        /// <summary>
        /// Добавление нового процесса в память
        /// </summary>
        /// <param name="processName">Название процесса</param>
        /// <param name="size">Размер сегмента с данными (в байтах)</param>
        public void AddProcess(string processName, int size)
        {
            // выделение памяти под новый процесс
            var process = new Process(processName);
            process.Segments = new Segment[2];
            try
            {
                process.Segments[0] = _mem.AllocateSegment(Vars.SegmentSize, String.Format("{0}_Code", processName));
                process.Segments[1] = _mem.AllocateSegment(size, String.Format("{0}_Data", processName));
                Processes.Add(process);
            }
            catch (Exception ex)
            {
                // удаление сегментов, выделенных процессу
                if (process.Segments[0] != null)
                    _mem.FreeSegment(process.Segments[0].Name);
                if (process.Segments[1] != null)
                    _mem.FreeSegment(process.Segments[1].Name);
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Удаление процесса из памяти
        /// </summary>
        /// <param name="processName">Название процесса</param>
        public bool RemoveProcess(string processName)
        {
            var process = Processes.SingleOrDefault(p => p.Name == processName);
            if (process != null)
            {
                _mem.FreeSegment(process.Segments[0].Name);
                _mem.FreeSegment(process.Segments[1].Name);
                Processes.Remove(process);

                return true;
            }
            return false;
        }

        /// <summary>
        /// Выполнение чтение из памяти из заданного сегмента
        /// </summary>
        /// <param name="segmentName">Имя сегмента</param>
        /// <param name="offset">Смещение</param>
        /// <param name="data">Данные для </param>
        public string ReadData(string segmentName, int offset, string data)
        {
            int length = Convert.ToInt32(data);
            string msg = ""; // исходная строка
            try
            {
                for (int i = 0; i < length; i++)
                    msg += _mem.Read(segmentName, offset + i);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return msg;
        }

        /// <summary>
        /// Выполнение записи в память в заданный сегмент
        /// </summary>
        /// <param name="segmentName">Имя сегмента</param>
        /// <param name="offset">Смещение</param>
        /// <param name="data">Данные для записи</param>
        public void WriteData(string segmentName, int offset, string data)
        {
            try
            {
                for (int i = 0; i < data.Length; i++)
                {
                    _mem.Write(segmentName, offset, data[i]);
                    offset++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Cвопинг процесса (сохренение процесса в файл на диске)
        /// </summary>
        /// <param name="index">Индекс процесса в списке процессов</param>
        public void SwopProcess(int index)
        {
            var process = Processes[index];
            if (process != null)
            {
                // сохранение сегментов процесса в свопе

                // очистка памяти от данных в сегментах
                _mem.FreeSegment(process.Segments[0].Name);
                _mem.FreeSegment(process.Segments[1].Name);
                // удаление процесса из памяти
                Processes.Remove(process);
                // помещение его в список процессов в свопе
                ProcessInSwop.Add(process);
            }
        }

        /// <summary>
        /// Cвопинг процесса (сохренение процесса в файл на диске)
        /// </summary>
        /// <param name="processName">Название процесса</param>
        public void SwopProcess(string processName)
        {
            var process = Processes.SingleOrDefault(p => p.Name == processName);
            if (process != null)
            {
                // сохранение сегментов процесса в свопе

                // очистка памяти от данных в сегментах
                _mem.FreeSegment(process.Segments[0].Name);
                _mem.FreeSegment(process.Segments[1].Name);
                // удаление процесса из памяти
                Processes.Remove(process);
                // помещение его в список процессов в свопе
                ProcessInSwop.Add(process);
            }
        }

        /// <summary>
        /// Освобождение памяти, занятой сегментом процесса
        /// </summary>
        /// <param name="segmentName">Название сегмента</param>
        public void FreeSegment(string segmentName)
        {
            _mem.FreeSegment(segmentName);
        }

        /// <summary>
        /// Очистка памяти
        /// </summary>
        public void Clear()
        {
            Processes.Clear();
            ProcessInSwop.Clear();
            _mem.Free();
        }
    }
}
