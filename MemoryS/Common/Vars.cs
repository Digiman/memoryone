﻿namespace MemoryS.Common
{
    /// <summary>
    /// Cтуктура с общими сведениями приложения
    /// </summary>
    public struct Vars
    {
        /// <summary>
        /// Полный размер памяти, заданный в стартовом окне
        /// </summary>
        public static int FullMemSize;

        /// <summary>
        /// Размер сегмента по умолчанию (по заданию)
        /// </summary>
        public const int SegmentSize = 3072; // 3 Кбайт
    }
}