﻿using System;
using MemoryS.Common;

namespace MemoryS.Memory
{
    /// <summary>
    /// Класс для реализации сегментной схемы памяти
    /// </summary>
    public class SegmentMemory
    {
        public int FreeMem; // объем свободной памяти (байт)
        private Segment[] _segments; // сегменты
        public readonly char[] MemoryData; // содержимое памяти

        /// <summary>
        /// Инициализация памяти с заданным размером
        /// </summary>
        /// <param name="size">Размер памяти</param>
        public SegmentMemory(int size)
        {
            MemoryData = new char[size * 1024]; // перевод в байты
            for (int i = 0; i < MemoryData.Length; i++)
                MemoryData[i] = '-';
            _segments = new Segment[0];
            FreeMem = size*1024;
        }
        
        /// <summary>
        /// Выделение памяти под сегмент
        /// </summary>
        /// <param name="size">Размер требуемого сегмента</param>
        /// <param name="name">Имя</param>
        /// <returns>Возвращает созданый сегмент</returns>
        public Segment AllocateSegment(int size, string name)
        {
            int ind; //индекс найденного сегмента
            if (FindSegment(name, out ind) != null)
                throw new Exception("Сегмент с таким именем уже существует!");
            if (FreeMem < size)
                throw new Exception("Закончилась свободная память!");

            var NewSegment = new Segment(name, size);
            NewSegment.IsFree = true; //новый сегмент свободен
            // изменение объемов памяти
            FreeMem -= size;
            // расширение массива сегментов
            Array.Resize(ref _segments, _segments.Length + 1);
            _segments[_segments.Length - 1] = NewSegment;
            return NewSegment;
        }
        
        /// <summary>
        /// Поиск сегмента в памяти
        /// </summary>
        /// <param name="segmentName">Имя сегмента</param>
        /// <param name="i">Индекс найденного сегмента</param>
        /// <returns>Возвращает найденный сегмент</returns>
        private Segment FindSegment(string segmentName, out int i)
        {
            for (i = 0; i < _segments.Length; i++)
                if (_segments[i].Name == segmentName)
                    break;
            if (_segments.Length > i)
                return _segments[i];
            return null;
        }
        
        /// <summary>
        /// Удаление сегмента из памяти
        /// </summary>
        /// <param name="name">Название сегмента</param>
        public void FreeSegment(string name)
        {
            int i;
            for (i = 0; i < _segments.Length; i++)
                if (_segments[i].Name == name)
                    break;
            if (_segments[i].Name != name)
                throw new Exception("Сегмент не найден");
            
            // освобождение ОЗУ
            FreeMem += _segments[i].Size;
            for (; i < _segments.Length - 1; i++)
                _segments[i] = _segments[i + 1];
            Array.Resize(ref _segments, _segments.Length - 1);
        }
        
        /// <summary>
        /// Очистка памяти
        /// </summary>
        public void Free()
        {
            FreeMem = Vars.FullMemSize * 1024;
            Array.Clear(_segments, 0, _segments.Length);
            _segments = new Segment[0];
            for (int i= 0; i < MemoryData.Length; i++)
                MemoryData[i] = '-';
        }
        
        /// <summary>
        /// Запись данных в память
        /// </summary>
        /// <param name="segmentName"></param>
        /// <param name="offset"></param>
        /// <param name="data"></param>
        public void Write(string segmentName, int offset, char data)
        {
            int ind;
            var S = FindSegment(segmentName, out ind);
            if (S == null)
                throw new Exception("Сегмент не найден!");
            if (offset > S.Size)
                throw new Exception("Смещение за пределами сегмента!");
            
            int GlobalOffset = CalcGlobalOffset(ind);
            int LocalOffset = offset % S.Size;
            MemoryData[GlobalOffset + LocalOffset] = data;
        }
        
        /// <summary>
        /// Чтение данных из памяти
        /// </summary>
        /// <param name="segmentName">Имя сегмента</param>
        /// <param name="offset">Смещение</param>
        /// <returns>Возвращает прочитанный симовол</returns>
        public char Read(string segmentName, int offset)
        {
            int ind;
            var S = FindSegment(segmentName, out ind);
            if (S == null)
                throw new Exception("Сегмент не найден!");
            if (offset > S.Size)
                throw new Exception("Смещение за пределами сегмента!");
            
            int GlobalOffset = CalcGlobalOffset(ind);
            int LocalOffset = offset % S.Size;
            return MemoryData[GlobalOffset + LocalOffset];
        }
        
        /// <summary>
        /// Вычисление глобального смещения
        /// </summary>
        /// <param name="ind">Индекс сегмента</param>
        /// <returns>Возвращает величину смещения</returns>
        private int CalcGlobalOffset(int ind)
        {
            int sum = 0;
            for (int i = 0; i < ind; i++)
            {
                sum += _segments[i].Size;
            }
            return sum;
        }
    }
}