﻿namespace MemoryS.Memory
{
    /// <summary>
    /// Класс для реализации сегмента
    /// </summary>
    public class Segment
    {
        public string Name; // название сегмента
        public int Size; // размер сегмента (байт)
        public bool IsFree; // состояние сегмента (занят или свободен)

        /// <summary>
        /// Инициализация сегмента
        /// </summary>
        /// <param name="sname">Название</param>
        /// <param name="ssize">Размер</param>
        public Segment(string sname, int ssize)
        {
            Name = sname;
            Size = ssize;
        }
    }
}