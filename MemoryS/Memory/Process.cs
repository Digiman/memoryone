﻿namespace MemoryS.Memory
{
    /// <summary>
    /// Класс для реализации процесса
    /// </summary>
    public class Process
    {
        /// <summary>
        /// Имя процесса
        /// </summary>
        public string Name;
        /// <summary>
        /// Сегменты процесса
        /// </summary>
        public Segment[] Segments;

        /// <summary>
        /// Инициализация процесса
        /// </summary>
        /// <param name="name">Имя процесса</param>
        public Process(string name)
        {
            Name = name;
            Segments = new Segment[0];
        }
    }
}