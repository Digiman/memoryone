﻿namespace MemoryP.Common
{
    /// <summary>
    /// Cтуктура с общими сведениями приложения
    /// </summary>
    public struct Vars
    {
        /// <summary>
        /// Полный размер памяти, заданный в стартовом окне
        /// </summary>
        public static int FullMemSize;

        /// <summary>
        /// Размер страницы по умолчанию (по заданию)
        /// </summary>
        public const int PageSize = 2048; // 2 Кбайт
    }
}