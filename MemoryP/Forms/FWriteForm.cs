﻿using System;
using System.Windows.Forms;

namespace MemoryP.Forms
{
    /// <summary>
    /// Форма для задания данных для записи в сегмент памяти
    /// </summary>
    public partial class FWriteForm : Form
    {
        /// <summary>
        /// Название страницы
        /// </summary>
        public string PageName;
        /// <summary>
        /// Смещение в сегменте
        /// </summary>
        public int Offset;
        /// <summary>
        /// Данные
        /// </summary>
        public string Data;

        /// <summary>
        /// Инициализация формы
        /// </summary>
        public FWriteForm()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Инициализация формы для записи
        /// </summary>
        public void InitAsWriteForm()
        {
            Text = "Запись";
            label3.Text = "Данные";
        }
        
        /// <summary>
        /// Инициализация формы для чтение 
        /// </summary>
        public void InitAsReadForm()
        {
            Text = "Чтение";
            label3.Text = "Длина";
        }

        /// <summary>
        /// Нажатие на кнопку ОК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                PageName = textBox1.Text;
                Offset = Convert.ToInt32(numericUpDown1.Value);
                Data = textBox2.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Не введены данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        
        /// <summary>
        /// Нажатие на кнопку Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
