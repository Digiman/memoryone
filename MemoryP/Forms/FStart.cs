﻿using System;
using System.Windows.Forms;
using MemoryP.Common;

namespace MemoryP.Forms
{
    /// <summary>
    /// Стартовая форма с заданием размера памяти
    /// </summary>
    public partial class FStart : Form
    {
        public FStart()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Нажатие кнопки Далее
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Vars.FullMemSize = Convert.ToInt32(textBox1.Text);
            this.Close();
        }
    }
}
