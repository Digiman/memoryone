﻿using System;
using MemoryP.Common;

namespace MemoryP.Memory
{
    /// <summary>
    /// Класс для реализации сегментной схемы памяти
    /// </summary>
    public class PageMemory
    {
        public int FreeMem; // объем свободной памяти (байт)
        private Page[] _pages; // страницы
        public readonly char[] MemoryData; // содержимое памяти

        /// <summary>
        /// Инициализация памяти с заданным размером
        /// </summary>
        /// <param name="size">Размер памяти (в Кбайт)</param>
        public PageMemory(int size)
        {
            MemoryData = new char[size * 1024]; // перевод в байты
            for (int i = 0; i < MemoryData.Length; i++)
                MemoryData[i] = '-';
            _pages = new Page[0];
            FreeMem = size*1024;
        }

        /// <summary>
        /// Выделение памяти под сегмент
        /// </summary>
        /// <param name="size">Размер требуемого сегмента</param>
        /// <param name="name">Имя</param>
        /// <returns>Возвращает созданый сегмент</returns>
        public Page AllocatePage(int size, string name)
        {
            int ind; //индекс найденного сегмента
            if (FindPage(name, out ind) != null)
                throw new Exception("Страница с таким именем уже существует!");
            if (FreeMem < size)
                throw new Exception("Закончилась свободная память!");

            var newPage = new Page(name, _pages.Length + 1, size);

            newPage.IsFree = true; //новый сегмент свободен
            // изменение объемов памяти
            FreeMem -= size;
            // расширение массива сегментов
            Array.Resize(ref _pages, _pages.Length + 1);
            _pages[_pages.Length - 1] = newPage;
            return newPage;
        }

        /// <summary>
        /// Поиск страницы в памяти
        /// </summary>
        /// <param name="pageName">Имя страницы</param>
        /// <param name="i">Индекс найденного сегмента</param>
        /// <returns>Возвращает найденный сегмент</returns>
        private Page FindPage(string pageName, out int i)
        {
            for (i = 0; i < _pages.Length; i++)
                if (_pages[i].Name == pageName)
                    break;
            if (_pages.Length > i)
                return _pages[i];
            return null;
        }

        /// <summary>
        /// Удаление сегмента из памяти
        /// </summary>
        /// <param name="name">Название страницы</param>
        public void FreePage(string name)
        {
            int i;
            for (i = 0; i < _pages.Length; i++)
                if (_pages[i].Name == name)
                    break;
            if (_pages[i].Name != name)
                throw new Exception("Страница не найдена!");

            // запись пустых значений в пямять на место найденной страницы
            int globalOffset = CalcGlobalOffset(i);
            for (int j = globalOffset; j < globalOffset + _pages[i].Size; j++)
            {
                MemoryData[j] = '-';
            }

            // освобождение ОЗУ
            FreeMem += _pages[i].Size;
            for (; i < _pages.Length - 1; i++)
                _pages[i] = _pages[i + 1];
            Array.Resize(ref _pages, _pages.Length - 1);
        }

        /// <summary>
        /// Очистка памяти
        /// </summary>
        public void Free()
        {
            FreeMem = Vars.FullMemSize*1024;
            Array.Clear(_pages, 0, _pages.Length);
            _pages = new Page[0];
            for (int i = 0; i < MemoryData.Length; i++)
                MemoryData[i] = '-';
        }

        /// <summary>
        /// Запись данных в память
        /// </summary>
        /// <param name="pageName">Название страницы</param>
        /// <param name="offset">Смещение в рамках страницы</param>
        /// <param name="data">Данные для записи</param>
        public void Write(string pageName, int offset, char data)
        {
            int ind;
            var S = FindPage(pageName, out ind);
            if (S == null)
                throw new Exception("Страница не найдена!");
            if (offset > S.Size)
                throw new Exception("Смещение за пределами страницы!");
            
            int globalOffset = CalcGlobalOffset(ind);
            int localOffset = offset % S.Size;
            MemoryData[globalOffset + localOffset] = data;
        }
        
        /// <summary>
        /// Чтение данных из памяти
        /// </summary>
        /// <param name="pageName">Имя страницы</param>
        /// <param name="offset">Смещение в рамках страницы</param>
        /// <returns>Возвращает прочитанный симовол</returns>
        public char Read(string pageName, int offset)
        {
            int ind;
            var S = FindPage(pageName, out ind);
            if (S == null)
                throw new Exception("Страница не найдена!");
            if (offset > S.Size)
                throw new Exception("Смещение за пределами страницы!");
            
            int globalOffset = CalcGlobalOffset(ind);
            int localOffset = offset % S.Size;
            return MemoryData[globalOffset + localOffset];
        }
        
        /// <summary>
        /// Вычисление глобального смещения
        /// </summary>
        /// <param name="ind">Индекс стрнаицы</param>
        /// <returns>Возвращает величину смещения</returns>
        private int CalcGlobalOffset(int ind)
        {
            int sum = 0;
            for (int i = 0; i < ind; i++)
            {
                sum += _pages[i].Size;
            }
            return sum;
        }

        /// <summary>
        /// Количество страниц в памяти
        /// </summary>
        public int PageCount
        {
            get { return _pages.Length; }
        }
    }
}