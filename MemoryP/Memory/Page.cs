﻿namespace MemoryP.Memory
{
    /// <summary>
    /// Класс для описания страницы памяти
    /// </summary>
    public class Page
    {
        public int Id; // номер страницы
        public string Name; // названеи страницы
        public int Size; // размер сегмента (байт)
        public bool IsFree; // состояние сегмента (занят или свободен)

        /// <summary>
        /// Инициализация сегмента
        /// </summary>
        /// <param name="id">Название</param>
        /// <param name="size">Размер страницы</param>
        public Page(int id, int size)
        {
            Id = id;
            Size = size;
        }

        /// <summary>
        /// Инициализация сегмента
        /// </summary>
        /// <param name="name">Название страницы</param>
        /// <param name="id">Номер страницы</param>
        /// <param name="size">Размер страницы</param>
        public Page(string name, int id, int size)
        {
            Name = name;
            Size = size;
        }
    }
}
