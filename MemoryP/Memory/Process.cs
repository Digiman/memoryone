﻿using MemoryP.Memory;

namespace MemorypP.Memory
{
    /// <summary>
    /// Класс для реализации процесса
    /// </summary>
    public class Process
    {
        /// <summary>
        /// Имя процесса
        /// </summary>
        public string Name;
        /// <summary>
        /// Сегменты процесса
        /// </summary>
        public Page[] Pages;

        /// <summary>
        /// Инициализация процесса
        /// </summary>
        /// <param name="name">Имя процесса</param>
        public Process(string name)
        {
            Name = name;
            Pages = new Page[0];
        }
    }
}