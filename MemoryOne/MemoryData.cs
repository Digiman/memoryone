﻿using MemoryOne.Disk;
using MemoryOne.Memory;

namespace MemoryOne
{
    /// <summary>
    /// Класс для хранения данных памяти и диска
    /// </summary>
    public static class MemoryData
    {
        public static MemoryBase Memory;
        public static SwapDisk Disk;

        /// <summary>
        /// Инициализация памяти с фиксированными разделами
        /// </summary>
        /// <param name="size">Размер памяти (единиц)</param>
        /// <param name="sections">Количество фиксированных разделов</param>
        public static void InitFixedMemory(int size, int sections)
        {
            Disk = new SwapDisk(size);
            Memory = new FixedMemory(size, Disk, sections);
        }

        /// <summary>
        /// Инициализация памяти с переменными разделами
        /// </summary>
        /// <param name="size">Размер памяти (единиц)</param>
        public static void InitVariableMemory(int size)
        {
            Disk = new SwapDisk(size);
            Memory = new VariableMemory(size, Disk);
        }
    }
}
