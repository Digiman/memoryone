﻿using System;
using System.Windows.Forms;

namespace MemoryOne.Forms
{
    /// <summary>
    /// Форма для ввода параметров удаляемого процесса
    /// </summary>
    public partial class RemoveProcess : Form
    {
        public RemoveProcess()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Нажатие кнопки Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
