﻿using System;
using System.Windows.Forms;

namespace MemoryOne.Forms
{
    /// <summary>
    /// Главная форма приложения
    /// </summary>
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Клик по кнопке Выход
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Клик по кнопке Добавить процесс
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            AddProcess frm = new AddProcess();
            frm.ShowDialog();
        }

        /// <summary>
        /// Клик по кнопке Очистка памяти
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show("Действительно очистить память?", "Подтверждение очистки", MessageBoxButtons.YesNoCancel,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                MemoryData.Memory.Clear();
                // TODO: здесь выполнить перерисовку картинки с памятью
                MessageBox.Show("Память успешно очищена!", "Подтверждение очистки", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Клик по кнопке Сжатие
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            // сжатие памяти нужно не всегда, например только для схемы с переменными разделами (есть смысл сделать кнопку только там)
        }

        /// <summary>
        /// Нажатие на кнопку Свопинг
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            // здесь пока ничего не нужно, т.к. свопинг будет происходить автоматически при добавлении. здесь же можно показать вручную как это работает для любого процесса
        }

        /// <summary>
        /// Нажатие на кнопку Удалить процесс
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            RemoveProcess frm = new RemoveProcess();
            frm.ShowDialog();
        }

        /// <summary>
        /// Обработка события закрытия формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (
                MessageBox.Show("Действительно выйти из программы?", "Подтверждение выхода",
                                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
