﻿using System;
using System.Windows.Forms;

namespace MemoryOne.Forms
{
    /// <summary>
    /// Форма для создания и добавления нового процесса
    /// </summary>
    public partial class AddProcess : Form
    {
        public AddProcess()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Нажатие кнопки Отмена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Нажатие кнопки ОК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox1.Text))
            {
                var p = new Process(textBox1.Text, (int)numericUpDown1.Value);
                MemoryData.Memory.Add(p);
                MessageBox.Show("Новый процесс успешно добавлен!", "Информация", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Не заданы параметры процесса!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
