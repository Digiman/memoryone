﻿using System.Collections.Generic;
using System.Linq;
using MemoryOne.Disk;

namespace MemoryOne.Memory
{
    /// <summary>
    /// Базовый класс для работы с памятью
    /// </summary>
    public abstract class MemoryBase
    {
        protected readonly List<Process> Processes;
        protected int Size;
        protected SwapDisk Disk;

        /// <summary>
        /// Инициализация памяти
        /// </summary>
        /// <param name="size">Объем памяти</param>
        /// <param name="disk">Диск для свопинга</param>
        protected MemoryBase(int size, SwapDisk disk)
        {
            Size = size;
            Processes = new List<Process>();
            Disk = disk;
        }

        /// <summary>
        /// Помещение процесса в память
        /// </summary>
        /// <param name="process">Новый процесс</param>
        /// <returns>Возвращает значение True, если процесс успешно добавлен, иначе False</returns>
        public virtual bool Add(Process process)
        {
            return false;
        }

        /// <summary>
        /// Удаление процесса из памяти
        /// </summary>
        /// <param name="name">Имя процесса</param>
        public void Remove(string name)
        {
            Processes.Remove(Processes.SingleOrDefault(p => p.Name == name));
        }

        /// <summary>
        /// Очистка пямяти
        /// </summary>
        /// <param name="withDisk">Чистить ли диск для свопинга тоже?</param>
        public void Clear(bool withDisk = true)
        {
            Processes.Clear();
            if (withDisk) Disk.Clear();
        }
    }
}
