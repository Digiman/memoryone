﻿using System.Collections.Generic;
using MemoryOne.Disk;

namespace MemoryOne.Memory
{
    /// <summary>
    /// Память с переменными разделами
    /// </summary>
    public class VariableMemory : MemoryBase
    {
        /// <summary>
        /// Список разделов
        /// </summary>
        private List<MemorySection> _sections;

        /// <summary>
        /// Инициализация памяти со схемой с переменными разделами
        /// </summary>
        /// <param name="size"></param>
        /// <param name="disk"></param>
        public VariableMemory(int size, SwapDisk disk)
            : base(size, disk)
        {
            _sections = new List<MemorySection>();
        }

        /// <summary>
        /// Помещение процесса в память
        /// </summary>
        /// <param name="process">Новый процесс</param>
        /// <returns>Возвращает значение True, если процесс успешно добавлен, иначе False</returns>
        public override bool Add(Process process)
        {
            // TODO: добавление процесса в память с переменными разделами
            // здесь нужно реализовывать логику стратегий размещения и вызывать весь код, для последовательности проверки и помещения процесса в виде раздела в память
            return false;
        }

        /// <summary>
        /// Сжатие
        /// </summary>
        public void Compress()
        {
            //TODO: реализовать сжатие в схеме с переменными разделами   
        }

        /// <summary>
        /// Свопинг для заданного процесса
        /// </summary>
        /// <param name="process">Процесс для сброса на диск</param>
        public void Swap(Process process)
        {
            
        }
    }
}
