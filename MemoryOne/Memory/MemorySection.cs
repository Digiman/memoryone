﻿namespace MemoryOne.Memory
{
    /// <summary>
    /// Раздел для памяти, построенной на разделах
    /// -------------------------------------------
    /// Что такое раздел и что про него нужно знать?
    /// 
    /// Раздел это некоторый блок непрерывно расположенных байтов в памяти.
    /// Он имеет такие параметры:
    ///     - начало раздела (адрес, с которого начинаются его байты)
    ///     - длину (емкость, объем). 
    /// Используется для схемы с фиксированными и переменными разделами и хранит в себе, по сути, процесс.
    /// -------------------------------------------
    /// </summary>
    public class MemorySection
    {
        /// <summary>
        /// Номер позиции (адрес) начала раздела
        /// </summary>
        private int _start;
        /// <summary>
        /// Размер раздела
        /// </summary>
        private readonly int _size;
        /// <summary>
        /// Процесс, размещенный в разделе
        /// </summary>
        private Process _process;

        /// <summary>
        /// Инициализация раздела заданного размера
        /// </summary>
        /// <param name="size">Размер раздела</param>
        public MemorySection(int size)
        {
            _size = size;
        }

        /// <summary>
        /// Номер позиции (адрес) начала раздела
        /// </summary>
        /// <remarks>Может меняться при сжатии!</remarks>
        public int Start
        {
            get { return _start; }
            set { _start = value; }
        }

        /// <summary>
        /// Помещение процесса в раздел
        /// </summary>
        /// <param name="process">Процесс</param>
        public void SetProcess(Process process)
        {
            _process = process;
        }
        
        /// <summary>
        /// Получение сведений о свободном месте с разделе
        /// </summary>
        /// <returns>Возвращает значение свободного места</returns>
        public int GetFreeSpace()
        {
            return _size - _process.Size;
        }
    }
}
