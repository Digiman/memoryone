﻿using System.Collections.Generic;
using MemoryOne.Disk;

namespace MemoryOne.Memory
{
    /// <summary>
    /// Память с фиксированными разделами
    /// </summary>
    public class FixedMemory : MemoryBase
    {
        /// <summary>
        /// Количество разделов
        /// </summary>
        private int _sectionCount;
        /// <summary>
        /// Список разделов
        /// </summary>
        private List<MemorySection> _sections;

        /// <summary>
        /// Инициализация памяти с фиксированными разделами
        /// </summary>
        /// <param name="size">Размер памяти (единиц)</param>
        /// <param name="disk">Диск для свопинга</param>
        /// <param name="sectionCount">Количество разделов</param>
        public FixedMemory(int size, SwapDisk disk, int sectionCount)
            : base(size, disk)
        {
            _sectionCount = sectionCount;
        }

        /// <summary>
        /// Добавление процесса в память
        /// </summary>
        /// <param name="process">Новый процесс</param>
        public override bool Add(Process process)
        {
            // TODO: описание логики помещения процесса в память
            Processes.Add(process);

            return true;
        }
    }
}
