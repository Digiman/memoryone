﻿using System;
using System.Windows.Forms;
using MemoryOne.Forms;

namespace MemoryOne
{
    /// <summary>
    /// Главный класс приложения
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            InitData();

            Application.Run(new MainForm());
        }

        /// <summary>
        /// Инициализация памяти
        /// </summary>
        static void InitData()
        {
            // NOTE: здесь происходит инициализация нужного класса для того или иного типа памяти
            MemoryData.InitVariableMemory(20);
        }
    }
}
