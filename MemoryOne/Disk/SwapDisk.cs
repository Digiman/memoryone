﻿using System.Collections.Generic;
using System.Linq;

namespace MemoryOne.Disk
{
    /// <summary>
    /// Диск (для свопинга)
    /// </summary>
    public class SwapDisk
    {
        private readonly List<Process> _processes;
        private readonly int _size;

        /// <summary>
        /// Инициализация диска с заданням объемом
        /// </summary>
        /// <param name="size">Размер диска</param>
        public SwapDisk(int size)
        {
            _size = size;
            _processes = new List<Process>();
        }

        /// <summary>
        /// Помещение процесса на диск
        /// </summary>
        /// <param name="process">Процесс для сброса</param>
        /// <returns>Возвращает True, если процесс был успешно добавлен на диск, иначе False</returns>
        public bool Add(Process process)
        {
            if (GetFreeSize >= process.Size)
            {
                _processes.Add(process);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Получение процесса с диска по его названию
        /// </summary>
        /// <param name="name">Название процесса</param>
        /// <returns>Возвращает процесс по его имени</returns>
        public Process GetProcess(string name)
        {
            var prc = _processes.SingleOrDefault(p => p.Name == name);
            _processes.Remove(prc);
            return prc;
        }

        /// <summary>
        /// Получение одного процесса для объема меньше заданного
        /// </summary>
        /// <param name="size">Заданный размер для поиска</param>
        /// <returns>Возвращает первый массив из найденных меньших заданного объема</returns>
        public Process GetProcess(int size)
        {
            var prc = _processes.FirstOrDefault(p => (p.Size <= size));
            _processes.Remove(prc);
            return prc;
        }

        /// <summary>
        /// Очистка диск от прроцессов
        /// </summary>
        public void Clear()
        {
            _processes.Clear();
        }

        /// <summary>
        /// Получение сводного места в своп-файле на диске
        /// </summary>
        /// <returns>Возвращает величину незаняттного пространства</returns>
        public int GetFreeSize
        {
            get
            {
                int sum = _processes.Sum(process => process.Size);
                return _size - sum;
            }
        }

        /// <summary>
        /// Количество процессов на диске
        /// </summary>
        public int GetProcessCount
        {
            get { return _processes.Count; }
        }
    }
}
