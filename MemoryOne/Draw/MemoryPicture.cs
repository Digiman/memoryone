﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryOne.Draw
{
    /// <summary>
    /// Класс для реализации рисования картинки с памятью
    /// </summary>
    public class MemoryPicture
    {
        private Graphics _graphics;

        public MemoryPicture(Graphics graphics)
        {
            _graphics = graphics;
        }

        /// <summary>
        /// Отрисовка содержимого памяти и диска
        /// </summary>
        public void Draw()
        {
            
        }

        /// <summary>
        /// Рисование памяти и процессов в ней
        /// </summary>
        private void DrawMemory()
        {
            
        }
        
        /// <summary>
        /// Рисование диска и процессов на нем
        /// </summary>
        public void DrawDisk()
        {
            
        }
    }
}
