﻿using System;
using System.Collections.Generic;
using System.IO;
using MemoryOne.Enums;

namespace MemoryOne.Helpers
{
    /// <summary>
    /// Класс для реализации лога работы с памятью
    /// </summary>
    /// <returns>Простой класс для реализации функции работы с логами, хранения их и
    /// записи в файл по окончании работы приложения</returns>
    public static class MemoryLog
    {
        /// <summary>
        /// Список сообщение в логе
        /// </summary>
        private static readonly List<string> Logs = new List<string>();

        /// <summary>
        /// Логтрование действия
        /// </summary>
        /// <param name="type">Тип</param>
        /// <param name="processName">Имя процесса</param>
        public static void Log(LogType type, string processName)
        {
            Logs.Add(GenerateMessage(type, processName));
        }

        /// <summary>
        /// Вставка сообщения в лог
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        public static void Log(string message)
        {
            Logs.Add(message);
        }

        private static string GenerateMessage(LogType type, string message)
        {
            return String.Format("Произошло действие: {0} для процесса {1}");
        }

        /// <summary>
        /// Сохранение лога в файл
        /// </summary>
        /// <param name="filename">Имя файла для сохранения</param>
        public static void SaveToFile(string filename)
        {
            var file = new StreamWriter(filename);

            foreach (var log in Logs)
            {
                file.WriteLine(log);
            }

            file.Close();
        }
    }
}
