﻿namespace MemoryOne
{
    /// <summary>
    /// 
    /// </summary>
    public class Process
    {
        private string _name;
        private int _size;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="count"></param>
        public Process(string name, int count)
        {
            _name = name;
            _size = count;
        }

        /// <summary>
        /// Количество памяти, используемое процессом
        /// </summary>
        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

        /// <summary>
        /// Название процесса
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
