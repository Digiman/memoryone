﻿namespace MemoryOne.Enums
{
    /// <summary>
    /// Тип события для лога
    /// </summary>
    public enum LogType
    {
        AddProcess, RemoveProcess, 
        SwapProcess, LoadProcessFromSwap,
        Compress
    }
}
